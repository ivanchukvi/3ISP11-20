# 3ISP11-20Test

14.09.23

## Настольное приложение разрабатывается для продаж строительных материалов и инструментов

## ТЗ
## https://gitlab.com/ivanchukvi/3isp11-20test/-/blob/main/ProjectPlan/%D0%A2%D0%97_%D0%90%D0%B4%D1%8B%D1%88%D0%BA%D0%B8%D0%BD%D1%83.docx

## База Данных
## https://gitlab.com/ivanchukvi/3isp11-20test/-/blob/main/ProjectPlan/DB.PNG

## UseCase
## https://gitlab.com/ivanchukvi/3isp11-20test/-/blob/main/ProjectPlan/UseCase.PNG

## Auth
## https://gitlab.com/ivanchukvi/3isp11-20test/-/blob/main/ProjectPlan/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA.PNG?ref_type=heads

## Products
## https://gitlab.com/ivanchukvi/3isp11-20test/-/blob/main/ProjectPlan/%D0%9F%D1%80%D0%BE%D0%B4%D1%83%D0%BA%D1%82%D1%8B.PNG

## Диаграмма последовательности
## https://gitlab.com/ivanchukvi/3isp11-20test/-/blob/main/ProjectPlan/%D0%94%D0%BC%D0%B0%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0_%D0%BF%D0%BE%D1%81%D0%BB%D0%B5%D0%B4%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D0%BE%D1%81%D1%82%D0%B8.png

## Диаграмма классов
## 

## Диаграмма деятельности
## https://gitlab.com/ivanchukvi/3isp11-20test/-/blob/main/ProjectPlan/%D0%94%D0%B8%D0%B0%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0_%D0%B0%D0%BA%D1%82%D0%B8%D0%B2%D0%BD%D0%BE%D1%81%D1%82%D0%B8.PNG
